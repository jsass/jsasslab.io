# Changelog

Please see the [releases page on Github](https://gitlab.com/jsass/jsass/-/releases) for the latest changelog.
