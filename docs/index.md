<div style="text-align: center" markdown>

# Welcome to jsass's documentation!

![jsass](assets/logo.svg)

</div>

<div class="grid cards" markdown>

-   ## jsass 6

    ---

    The ultimate [SASS](https://sass-lang.com/) compiler for Java / the JVM.

    [:material-arrow-right-thin: start with jsass 6](jsass6)

-   ## jsass 5 (EOL)

    ---

    The [sass](https://sass-lang.com/) compiler for java using [libsass](https://sass-lang.com/libsass/).

    [:material-arrow-right-thin: start with jsass 5](jsass5)

</div>
