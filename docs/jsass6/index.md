# About

![jsass](../assets/logo.svg)

The ultimate [SASS](https://sass-lang.com/) compiler for Java / the JVM.

!!! warning

    Jsass 6 is still in the early stages of development. API breaks are to be expected!

*:material-timer-sand: comming soon, stay tuned...*
