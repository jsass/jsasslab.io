!!! warning

    Jsass 6 is still in the early stages of development. API breaks are to be expected!

# Architecture

Preview of the technical architecture.

```mermaid
classDiagram
  class JsassCompiler
  <<interface>> JsassCompiler
  JsassCompiler : +compileString(@NotNull String source, @Nullable Options options) CompletableFuture~Output~
  
  class Output
  Output : String css
  Output : String sourceMap
  
  class JavetV8JsassCompiler
  JsassCompiler <|-- JavetV8JsassCompiler
  
  class JavetNodeJsassCompiler
  JsassCompiler <|-- JavetNodeJsassCompiler
  
  class EmbeddedSassJsassCompiler
  JsassCompiler <|-- EmbeddedSassJsassCompiler
```

## JsassCompiler Interface

The JsassCompiler interface is the new central entry point.
Various implementations with different backends are available.

## Backends

### JavetV8JsassCompiler

!!! info

    In early development!

The `JavetV8JsassCompiler` backend uses [Javet] in V8 Mode to run [dart-sass]. This mode can be flexibly expanded with additional ECMAScript modules.

### JavetNodeJsassCompiler

!!! info

    Conception phase!
    
The `JavetNodeJsassCompiler` backend uses [Javet] in Node mode to run [dart-sass]. This mode can be flexibly expanded with additional NPM modules.

### EmbeddedSassJsassCompiler

!!! info

    Conception phase!

The `EmbeddedSassJsassCompiler` backend uses the [Embedded Sass Protocol](https://github.com/sass/sass/blob/main/spec/embedded-protocol.md) to delegate the compile process. This mode effectively has the smallest resource footprint and requires an embedded Sass service.

---

*More backends are being planned :partying_face:*

[Javet]: https://www.caoccao.com/Javet/index.html
[dart-sass]: https://sass-lang.com/dart-sass/
