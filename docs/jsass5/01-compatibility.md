# Compatibility

On this page, you find an overview of system compatibility.

## :material-language-java: Java

- <span style="color:red">:x: Java 7 unsupported</span>
- <span style="color:green">:white_check_mark: Java 8 supported</span>
- <span style="color:green">:white_check_mark: Java 11 supported</span>

## :simple-linux: Linux

<div class="annotate" markdown>

- <span style="color:chocolate">:white_check_mark: Linux x86 partially supported (1)</span>
- <span style="color:green">:white_check_mark: Linux x86_64 (amd64) supported</span>
- <span style="color:blueviolet">:white_check_mark: Linux armhf32 experimental (2)</span>
- <span style="color:green">:white_check_mark: Linux aarch64 supported</span>

</div>

1.  since jsass 5.7.4 the 32bit support was been removed
2.  experimental, tests still failing

## :material-microsoft-windows: Windows

- <span style="color:red">:x: Windows x86 unsupported</span>
- <span style="color:green">:white_check_mark: Windows x86_64 (amd64) supported</span>
- <span style="color:red">:x: Windows armhf32 unsupported</span>
- <span style="color:red">:x: Windows aarch64 unsupported</span>

## :simple-apple: MacOS

<div class="annotate" markdown>

- <span style="color:green">:white_check_mark: MacOS Intel supported</span>
- <span style="color:green">:white_check_mark: MacOS Apple Silicon unsupported (1)</span>

</div>

1. since jsass 5.11

## Others

- <span style="color:red">:simple-oracle: Solaris unsupported</span>
- <span style="color:red">:simple-freebsd: FreeBSD unsupported</span>
