# Basic examples

## Compile file

Compiling a file is pretty simple, give an input file and an output file, the rest is just magic.

```java title="Compile a file (basic usage)"
import static java.lang.System.err;
import static java.lang.System.out;

import io.bit3.jsass.CompilationException;
import io.bit3.jsass.Compiler;
import io.bit3.jsass.Options;
import io.bit3.jsass.Output;

import java.io.File;
import java.net.URI;

public class CompileFileExample {
  public static void main(String[] args) {
    final var inputFile = new File("stylesheet.scss").toURI();
    final var outputFile = new File("stylesheet.css").toURI();

    final var compiler = new Compiler();
    final var options = new Options();

    try {
      final var output = compiler.compileFile(inputFile, outputFile, options);

      out.println("Compiled successfully");
      out.println(output.getCss());
    } catch (CompilationException e) {
      err.println("Compile failed");
      err.println(e.getErrorText());
    }
  }
}
```

```java title="Compile a file context (advanced usage)"
import static java.lang.System.err;
import static java.lang.System.out;

import io.bit3.jsass.CompilationException;
import io.bit3.jsass.Compiler;
import io.bit3.jsass.Options;
import io.bit3.jsass.Output;
import io.bit3.jsass.context.FileContext;

import java.io.File;
import java.net.URI;

public class CompileFileContextExample {
  public static void main(String[] args) {
    final var inputFile = new File("stylesheet.scss").toURI();
    final var outputFile = new File("stylesheet.css").toURI();

    final var compiler = new Compiler();
    final var options = new Options();

    try {
      final var context = new FileContext(inputFile, outputFile, options);
      final var output = compiler.compile(context);

      out.println("Compiled successfully");
      out.println(output.getCss());
    } catch (CompilationException e) {
      err.println("Compile failed");
      err.println(e.getErrorText());
    }
  }
}
```

Compile string
--------------

Compiling a string is pretty simple, give an input string, the rest is just magic.
Providing an input file and output file is always a good idea. With this informations libsass can determine the default
include path and calculate relative paths.

```java title="Compile a string (basic usage)"
import static java.lang.System.err;
import static java.lang.System.out;

import io.bit3.jsass.CompilationException;
import io.bit3.jsass.Compiler;
import io.bit3.jsass.Options;
import io.bit3.jsass.Output;

import java.io.File;
import java.net.URI;

public class CompileStringExample {
  public static void main(String[] args) {
    final var input = "body { color: red; }";
    final var inputFile = new File("stylesheet.scss").toURI();
    final var outputFile = new File("stylesheet.css").toURI();

    final var compiler = new Compiler();
    final var options = new Options();

    try {
      final var output = compiler.compileString(input, inputFile, outputFile, options);

      out.println("Compiled successfully");
      out.println(output.getCss());
    } catch (CompilationException e) {
      err.println("Compile failed");
      err.println(e.getErrorText());
    }
  }
}
```

```java title="Compile a string context (advanced usage)"
import static java.lang.System.err;
import static java.lang.System.out;

import io.bit3.jsass.CompilationException;
import io.bit3.jsass.Compiler;
import io.bit3.jsass.Options;
import io.bit3.jsass.Output;
import io.bit3.jsass.context.StringContext;

import java.io.File;
import java.net.URI;

public class CompileStringContextExample {
  public static void main(String[] args) {
    final var input = "body { color: red; }";
    final var inputFile = new File("stylesheet.scss").toURI();
    final var outputFile = new File("stylesheet.css").toURI();

    final var compiler = new Compiler();
    final var options = new Options();

    try {
      final var context = new StringContext(input, inputFile, outputFile, options);
      final var output = compiler.compile(context);

      out.println("Compiled successfully");
      out.println(output.getCss());
    } catch (CompilationException e) {
      err.println("Compile failed");
      err.println(e.getErrorText());
    }
  }
}
```
