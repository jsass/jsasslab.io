# Testing Matrix

An overview of tested operating systems, architecture, and the corresponding CI system used.

| Operating System / Base Image                                    | Architecture   | CI System                                |
|------------------------------------------------------------------|----------------|------------------------------------------|
| :simple-centos: CentOS 7 [`centos:7`][centos]                    | x86_64 (amd64) | [:material-gitlab: Gitlab CI][gitlab-ci] |
| :simple-centos: CentOS 8 [`centos:8`][centos]                    | x86_64 (amd64) | [:material-gitlab: Gitlab CI][gitlab-ci] |
| :material-ubuntu: Ubuntu 16.04 [`ubuntu:16.04`][ubuntu]          | x86_64 (amd64) | [:material-gitlab: Gitlab CI][gitlab-ci] |
| :material-ubuntu: Ubuntu 18.04 [`ubuntu:18.04`][ubuntu]          | x86_64 (amd64) | [:material-gitlab: Gitlab CI][gitlab-ci] |
| :material-ubuntu: Ubuntu 20.04 [`ubuntu:20.04`][ubuntu]          | x86_64 (amd64) | [:material-gitlab: Gitlab CI][gitlab-ci] |
| :material-ubuntu: Ubuntu 22.04 [`ubuntu:22.04`][ubuntu]          | x86_64 (amd64) | [:material-gitlab: Gitlab CI][gitlab-ci] |
| :material-debian: Debian 10 Buster [`debian:buster`][debian]     | x86_64 (amd64) | [:material-gitlab: Gitlab CI][gitlab-ci] |                            
| :material-debian: Debian 11 Bullseye [`debian:bullseye`][debian] | x86_64 (amd64) | [:material-gitlab: Gitlab CI][gitlab-ci] |            
| :material-debian: Debian 12 Bookworm [`debian:bookworm`][debian] | x86_64 (amd64) | [:material-gitlab: Gitlab CI][gitlab-ci] |            
| :material-ubuntu: Ubuntu [`eclipse-temurin:11`][eclipse-temurin] | armhf32        | [:simple-drone: Drone.io][drone-io]      | 
| :material-ubuntu: Ubuntu [`eclipse-temurin:11`][eclipse-temurin] | aarch64        | [:simple-drone: Drone.io][drone-io]      | 
| ~~:material-microsoft-windows: Windows~~                         |                | ~~:simple-travisci: Travis CI~~ [^1]     |
| ~~:material-apple: macOS~~                                       |                | ~~:simple-travisci: Travis CI~~ [^1]     |

[^1]: Since Travis CI is no longer free, especially when using Windows and macOS, additional costs arise, and we no longer have active tests for these operating systems.

[gitlab-ci]: https://gitlab.com/jsass/jsass/-/pipelines/latest
[drone-io]: https://cloud.drone.io/bit3/jsass/latest

[centos]: https://hub.docker.com/_/centos
[ubuntu]: https://hub.docker.com/_/ubuntu
[debian]: https://hub.docker.com/_/debian
[debian]: https://hub.docker.com/_/debian
[eclipse-temurin]: https://hub.docker.com/_/eclipse-temurin
