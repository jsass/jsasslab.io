# About

![jsass](../assets/logo.svg)

jsass 5 is a Java [sass](https://sass-lang.com/) compiler using [libsass](https://sass-lang.com/libsass/).

## Attention: only maintenance, no further development

libsass has been [declared deprecated][libsass-deprecated] and there will be no further development on jsass 5!
Please take a look on [jsass 6](../jsass6/index.md)

[libsass-deprecated]: https://sass-lang.com/blog/libsass-is-deprecated
