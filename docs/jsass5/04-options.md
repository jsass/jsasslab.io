# Options

The options class allow to customize each compilation process. Most options are equals to the well known command line
options from sass compilers.

## Function providers

```java
options.getFunctionProviders().add(new MyFunctions());
```

## Headers

```java
options.getHeaderImporters().add(new MyHeaderImporter());
```

## Importers

```java
options.getImporters().add(new MyImporter());
```

## Include paths

```java
options.getIncludePaths().add(new File("bower_components/foundation/scss");
```

## Indention

```java
options.setIndent("\t");
```

## SASS syntax

Treat source_string as sass (as opposed to scss).

```java
options.setIsIndentedSyntaxSrc(true);
```

## Linefeed

```java
options.setLinefeed("\r\n");
```

## Omit source map url

Disable sourceMappingUrl in css output.

```java
options.setOmitSourceMapUrl(true);
```

## Output style

Output style for the generated css code.

```java
options.setOutputStyle(io.bit3.jsass.OutputStyle.NESTED);
```

## Precision

Precision for outputting fractional numbers.

```java
options.setPrecision(6);
```

## Inline source comments

If you want inline source comments.

```java
options.setSourceComments(true);
```

## Embed contents in source map

Embed include contents in maps.

```java
options.setSourceMapContents(true);
```

## Embedded source map

Embed sourceMappingUrl as data uri.

```java
options.setSourceMapEmbed(true);
```

## Source map

Path to source map file. Enables the source map generating. Used to create sourceMappingUrl.

```java
options.setSourceMapFile(new File("stylesheet.css.map");
```
