# jsass.gitlab.com

The official jsass documentation.

## Development

Serve using podman: `podman run --user="$UID:$UID" --userns=keep-id --rm --security-opt label=disable -p 8000:8000 -v $(pwd):/docs:z squidfunk/mkdocs-material`
